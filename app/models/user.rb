class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :lockable,  and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :timeoutable, :confirmable

  validates :first_name, presence: true, on: :update
  validates :last_name, presence: true, on: :update

  def complete_profile?
    first_name.blank? || last_name.blank? ? false : true
  end

  def full_name
    first_name + " " + last_name
  end
end
