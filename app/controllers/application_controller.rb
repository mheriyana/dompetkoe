class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :authenticate_user!, :completed_profile

  def completed_profile
    redirect_to  complete_profile_path unless current_user.complete_profile?
  end
end
