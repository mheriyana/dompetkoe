module ApplicationHelper

  def bootstrap_class_for type
    { success: "alert-success", error: "alert-danger", alert: "alert-warning", notice: "alert-info" }[type.to_sym]
  end

  def flash_messages
    flash.each do |key, message|
      concat(content_tag(:div, message, class: "alert #{bootstrap_class_for(key)} fade in") do
            concat content_tag(:button, 'x', class: "close", data: { dismiss: 'alert' })
            concat message
          end)
    end
    nil
  end

  def flash_notify opt = ''
    notify = ''
    [:success, :alert, :notice].each do |type|
      if flash[type]
        case opt
        when 'type'
          notify << bootstrap_class_for(type).gsub('alert-', '')
        when 'message'
          notify << flash[type]
        end
      end
    end
    notify
  end

  def nav_link(link_text, link_path, http_method=nil)
    class_name = current_page?(link_path) ? 'active' : ''

    content_tag(:li, class: class_name) do
      if http_method
        link_to(link_text, link_path, method: http_method)
      else
        link_to(link_text, link_path)
      end
    end
  end
end
