Rails.application.routes.draw do

  root 'dashboard#index'

  devise_for :users, skip: [:sessions, :registrations],
    controllers: {
      confirmations: 'users/confirmations',
      passwords: 'users/passwords'
    }
  as :user do
    # Sessions
    get 'sign_in', to: 'users/sessions#new', as: 'sign_in'
    post 'sign_in', to: 'users/sessions#create', as: 'user_session'
    delete 'sign_out', to: 'users/sessions#destroy'
    # registration
    get 'sign_up', to: 'users/registrations#new'
    post 'users', to: 'users/registrations#create', as: 'user_registration'
    get 'complete_profile', to: 'users/registrations#complete_profile', as: 'complete_profile'
    put 'users', to: 'users/registrations#update'
    get 'edit_user', to: 'users/registrations#edit'
  end

end
